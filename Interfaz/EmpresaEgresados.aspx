﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="EmpresaEgresados.aspx.cs" Inherits="Interfaz.EmpresaEgresados" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" />
    <link rel="stylesheet" href="https://fontawesome.com/icons/id-card?s=solid&f=classic" />
    <link rel="stylesheet" href="css/estiloslogin.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Ingresar nueva empresa</h1>
    <div class="contenedor">

        <div class="input-contenedor">
            <i class="fas fa-industry icon"></i>
            <asp:TextBox ID="txtNomempresa" runat="server" placeholder="Empresa"></asp:TextBox>
        </div>
        <div class="input-contenedor">
            <i class="fas fa-phone icon"></i>
            <asp:TextBox ID="txtTelefonoempresa" runat="server" placeholder="Teléfono empresa"></asp:TextBox>
        </div>
        <div class="input-contenedor">
            <i class='fas fa-envelope icon'></i>
            <asp:TextBox ID="txtCorreoempresa" runat="server" placeholder="Correo Empresa"></asp:TextBox>

        </div>
        <div class="input-contenedor">
            <i class="fas fa-browser icon"></i>
            <asp:TextBox ID="txtWebempresa" runat="server" placeholder="Sitio Web"></asp:TextBox>
        </div>
        <div class="input-contenedor">
            <i class="fas fa-servicestack icon"></i>
            <asp:TextBox ID="txtSector" runat="server" placeholder="Sector"></asp:TextBox>
        </div>





        <asp:Button ID="btnInscripcion" runat="server" Text="Registrar empresa" CssClass="button" OnClick="btnInscripcion_Click" />
        <br />
        <br />
        <asp:Button ID="btnHome" runat="server" Text="Regresar" CssClass="button" OnClick="btnHome_Click" />
    </div>
</asp:Content>
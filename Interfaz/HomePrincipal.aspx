﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HomePrincipal.aspx.cs" Inherits="Interfaz.HomePrincipal" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel ="stylesheet" type="text/css" href="css/styleHome.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
    <style type="text/css">
        .auto-style1 {
            height: 40px;
            
        }
    </style>
</head>
<body>
    <form id="form1" runat="server"> 
            <header>
            <div class="logo" align="center">
                <center><img src="img/logo.png" /></center>
            </div>
        </header>
        <nav class="auto-style1">
        <a href="#" class="arriba">
           </a><input type="checkbox" id="check"/>
        <label for="check" class="checkbtn">
            <i class="fas fa-bars"></i>
        </label>
         <ul>
            <li><a class="active">Inicio</a></li>
            <li><a href="registro.aspx">Registro</a></li>
            <li><a href="Opciondegraduacion.aspx">Inscripción</a></li>
            <li><a href="formegresados.aspx">Trazabilidad</a></li>
        </ul>
    </nav>
        <section class="content sau">
        <h2 >COLEGIO UNIVERSITARIO DE CARTAGO</h2>
            <br />
        <p>Estudiá una carrera con demanda laboral. ¡Matriculá hoy! Ofrecemos cursos a la comunidad: Incluye programas técnicos, técnicos y cursos libres. 
            Formamos líderes. Institución estatal. Carreras cortas. Programas: 
            Electrónica, Investigación Criminal, Turismo, Mecánica Dental, entre otros cursos libres que ofrecemos.</p>
        
        <div class="box-container">

            <div class="box">
                
                <h3>Proyecto de graduación</h3>
            </div>
            <div class="box">
                
                <h3>Practica Supervisada</h3>
            </div>
            <div class="box">
                
                <h3 >Examen Practico</h3>
            </div>

        </div>

    </section>

    <section class="content about">
        <br />
        <h2 class="title">¿Opción de Graduación?</h2>
        <br />
        <p>La opción de grado es la modalidad académica establecida para que el estudiante, 
            en la última etapa de sus estudios, fortalezca su formación profesional o disciplinaria. Es requisito para acceder al título profesional y
            demanda acompañamiento y dirección desde la respectiva unidad académica.
            El estudiante debe pagar derechos pecuniarios por la opción académica de grado, de acuerdo con lo determinado por la Universidad.
        </p>


    </section>

        <footer>
            <div class="generalWrap">
                <div>
                    <i class="fas fa-phone fa-fw"></i>
                    <span>2525-8700</span>
                </div>
                <div class="notranslate">
                    <i class="fas fa-envelope fa-fw"></i>
                    <span><a href="inscripcioncuc@gmail.com" class="mail">inscripcioncuc@gmail.com</a></span>
                </div>
                <div>
                    <i class="fas fa-clock fa-fw"></i>
                    <span>
                        Lunes a Viernes de 7 a.m. a 5:00 p.m.
                    </span>
                </div>
                <div>
                    <i class="fas fa-map-marked-alt fa-fw"></i>
                    <span>Cartago, Costa Rica</span>
                </div>
            </div>
        </footer>
    </form>
</body>
</html>

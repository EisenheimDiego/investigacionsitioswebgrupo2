﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Negocio;

namespace Interfaz
{
    public partial class EmpresaGraduacion : System.Web.UI.Page
    {
        InscripcionEstudiantes ie = new InscripcionEstudiantes();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInscripcion_Click(object sender, EventArgs e)
        {
            try
            {


                if (txtNombre.Text == " " || txtCorreo.Text == "" || txtDireccion.Text == " " || txtTelefono.Text == " " || txtEncargado.Text == "")
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Aviso!','No se permiten espacios en blanco!','warning')", true);
                }
                else
                {
                    ie.InsertarEmpresa(txtNombre.Text,txtDireccion.Text,txtCorreo.Text,txtTelefono.Text,txtEncargado.Text);
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Los datos fueron validados correctamente!','Registrado','success')", true);
                }
            }
            catch (Exception ex)
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Error al registrar los datos!','La empresa ha sido registrada anteriormente','error')", true);
            }
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("Opciondegraduacion.aspx");
        }
    }
}
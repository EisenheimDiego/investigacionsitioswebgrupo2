﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Opciondegraduacion.aspx.cs" Inherits="Interfaz.Opciondegraduacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel ="stylesheet" type="text/css" href="css/estilosTyS.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
    <style type="text/css">
        .auto-style2 {
            height: 126px;
            width: 100%;            
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="box-container">

            <a href="prueba.aspx" class="hiper">
                <i class="fas fa-file-alt"></i>
                <h4>Practica Supervisada</h4>
            </a>
            <a href="ogexamen.aspx" class="hiper">
                <i class="fas fa-file-alt"></i>
                <h4>Examen</h4>
            </a>
            <a href="ogproyecto.aspx" class="hiper">
                <i class="fas fa-file-alt"></i>
                <h4>Proyecto de Graduación</h4>
            </a>

        </div>
</asp:Content>

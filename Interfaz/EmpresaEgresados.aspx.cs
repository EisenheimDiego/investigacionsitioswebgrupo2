﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Negocio;
namespace Interfaz
{
    public partial class EmpresaEgresados : System.Web.UI.Page
    {
        TrazabilidadEgresados te = new TrazabilidadEgresados();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnInscripcion_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtNomempresa.Text == " " ||txtCorreoempresa.Text == "" || txtTelefonoempresa.Text == " " || txtWebempresa.Text == " " || txtSector.Text == "")
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Aviso!','No se permiten espacios en blanco!','warning')", true);
                }
                else
                {

                    te.InsertarEmpresaEgresado(txtNomempresa.Text, txtCorreoempresa.Text, txtTelefonoempresa.Text, txtWebempresa.Text, txtSector.Text);

                    ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Los datos fueron validados correctamente!','Registrado','success')", true);
                }
            }
            catch (Exception ex)
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Error al registrar los datos!','La empresa ya ha sido registrada anteriormente','error')", true);
            }
        }

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("Opciondegraduacion.aspx");
            
        }
    }
}
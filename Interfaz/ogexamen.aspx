﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="ogexamen.aspx.cs" Inherits="Interfaz.ogexamen" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" />
    <link rel="stylesheet" href="https://fontawesome.com/icons/id-card?s=solid&f=classic" />
	<link rel="stylesheet" href="css/estiloslogin.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.css" />
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <h1> Formulario de Examen</h1>
    <div class="contenedor">

         <div class="input-contenedor">         
             <i class='fas fa-id-card icon'></i>
             <asp:TextBox ID="txtCedula" runat="server" placeholder="Cédula" ></asp:TextBox>      
         </div>
          <div class="input-contenedor">         
             <i class='fas fa-id-card icon'></i>
             <asp:TextBox ID="txtAno" runat="server" placeholder="Año" ></asp:TextBox>      
         </div>
        <div class="input-contenedor">         
             <i class='fas fa-id-card icon'></i>
             <asp:DropDownList ID="ddlCuatri" runat="server">
                 <asp:ListItem>Elija un periodo...</asp:ListItem>
                 <asp:ListItem Value="Primero">Primer Cuatrimestre</asp:ListItem>
                 <asp:ListItem Value="Segundo">Segundo Cuatrimestre</asp:ListItem>
                 <asp:ListItem Value="Tercero">Tercer Cuatrimestre</asp:ListItem>
         
             </asp:DropDownList>
       </div>
         <div class="input-contenedor">         
             <i class='fas fa-id-card icon'></i>
             <asp:DropDownList ID="ddlCarreras" runat="server">
                 <asp:ListItem>Elige una carrera...</asp:ListItem>
                 <asp:ListItem Value="AE">Administración de Empresas</asp:ListItem>
                 <asp:ListItem Value="DT">Mecanica Dental</asp:ListItem>
                 <asp:ListItem Value="EL">Electronica</asp:ListItem>
                 <asp:ListItem Value="IC">Investigación Criminal</asp:ListItem>
                 <asp:ListItem Value="SE">Secretariado</asp:ListItem>
                 <asp:ListItem Value="TI">Tecnologías de la Información</asp:ListItem>
                 <asp:ListItem Value="TR">Turismo</asp:ListItem>
             </asp:DropDownList>
             

         </div>
         <div class="input-contenedor">         
             <i class='fas fa-id-card icon'></i>
             <asp:TextBox ID="txtTutor" runat="server" placeholder="ID Tutor" ></asp:TextBox>      
         </div>

     
      <!--  <div class="input-contenedor">
             <i class="fas fa-user icon"></i>
            <asp:TextBox ID="txtNombre" runat="server" placeholder="Nombre" ></asp:TextBox>      
         </div>

         <div class="input-contenedor">
             <i class="fas fa-user icon"></i>
             <asp:TextBox ID="txtPriApellido" runat="server" placeholder="Primer Apellido" ></asp:TextBox>              
         </div>

         <div class="input-contenedor">
             <i class="fas fa-user icon"></i>
             <asp:TextBox ID="txtSegApellido" runat="server" placeholder="Segundo Apellido" ></asp:TextBox>       
         </div>

        <div class="input-contenedor">
        <i class='fas fa-envelope icon'></i>
        <asp:TextBox ID="txtCorreo" runat="server" placeholder="Correo Electrónico" ></asp:TextBox>
         </div>
         
         <div class="input-contenedor">
         <i class='fas fa-phone icon'></i>
         <asp:TextBox ID="txtTelefono" runat="server" placeholder="Teléfono" ></asp:TextBox>
         </div>
         
         <div class="input-contenedor">
        <i class="fas fa-key icon"></i>
        <asp:TextBox ID="txtPassword" runat="server" placeholder="Contraseña" type="password"></asp:TextBox>         
         </div>-->

         <asp:Button ID="btnInscripcion" runat="server" Text="Inscribirse"  CssClass="button" OnClick="btnInscripcion_Click" />
        <br />  <br />  
         <asp:Button ID="btnHome" runat="server" Text="Home"  CssClass="button" OnClick="btnHome_Click" />
     </div>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="EmpresaGraduacion.aspx.cs" Inherits="Interfaz.EmpresaGraduacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" />
    <link rel="stylesheet" href="https://fontawesome.com/icons/id-card?s=solid&f=classic" />
    <link rel="stylesheet" href="css/estiloslogin.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Ingresar nueva empresa</h1>
    <div class="contenedor">

        <div class="input-contenedor">
            <i class='fas fa-user icon'></i>
            <asp:TextBox ID="txtNombre" runat="server" placeholder="Nombre de empresa"></asp:TextBox>
        </div>
        <div class="input-contenedor">
            <i class='fas fa-info icon'></i>
            <asp:TextBox ID="txtDireccion" runat="server" placeholder="Dirección las oficinas"></asp:TextBox>
        </div>
        <div class="input-contenedor">
            <i class='fas fa-envelope icon'></i>
            <asp:TextBox ID="txtCorreo" runat="server" placeholder="Correo Electrónico"></asp:TextBox>
        </div>
        <div class="input-contenedor">
            <i class="fas fa-phone icon"></i>
            <asp:TextBox ID="txtTelefono" runat="server" placeholder="Telefono de la empresa"></asp:TextBox>
        </div>
        <div class="input-contenedor">
            <i class="fas fa-user icon"></i>
            <asp:TextBox ID="txtEncargado" runat="server" placeholder="Encargado de la empresa"></asp:TextBox>
        </div>





        <asp:Button ID="btnInscripcion" runat="server" Text="Registrar empresa" CssClass="button" OnClick="btnInscripcion_Click" />
        <br />
        <br />
        <asp:Button ID="btnHome" runat="server" Text="Regresar" CssClass="button" OnClick="btnHome_Click" />
    </div>
</asp:Content>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Negocio;
namespace Interfaz
{
    public partial class prueba : System.Web.UI.Page
    {
        InscripcionEstudiantes ie = new InscripcionEstudiantes();

        protected void Page_Load(object sender, EventArgs e)
        {

        }
      

        protected void btnHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("HomePrincipal.aspx");

        }

        protected void btnInscripcion_Click(object sender, EventArgs e)
        {
            try
            {

            
                if (txtCedula.Text == " " || txtAno.Text == "" || ddlCarreras.Text == " " || txtTutor.Text == " " ||  txtEmpresa.Text == "")
                {
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Aviso!','No se permiten espacios en blanco!','warning')", true);
                }
                else
                {
                    ie.InsertarInscripcion(txtCedula.Text, Convert.ToInt16(txtAno.Text), ddlCuatri.SelectedValue, ddlCarreras.SelectedValue, txtTutor.Text, "PRS", txtEmpresa.Text);
                    ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Los datos fueron validados correctamente!','Registrado','success')", true);
                }
            }catch(Exception ex)
            {
                ClientScript.RegisterClientScriptBlock(this.GetType(), "messagebox", "swal('¡Error al registrar los datos!','Los datos ingresados ya han sido registrados','error')", true);
            }

        }




    }
}
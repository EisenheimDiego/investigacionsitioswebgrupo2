﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="formegresados.aspx.cs" Inherits="Interfaz.formegresados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" />
    <link rel="stylesheet" href="https://fontawesome.com/icons/id-card?s=solid&f=classic" />
    <link rel="stylesheet" href="https://fontawesome.com/icons/building?s=solid&f=classic" />
    <link rel="stylesheet" href="https://fontawesome.com/icons/phone?s=duotone&f=classic" />
    <link rel="stylesheet" href="https://fontawesome.com/icons/helmet-safety?s=solid&f=classic" />
    <link rel="stylesheet" href="https://fontawesome.com/icons/building-columns?s=solid&f=classic" />
    <link rel="stylesheet" href="https://fontawesome.com/icons/user-graduate?s=solid&f=classic" />



    <link rel="stylesheet" href="css/estiloslogin.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.0.1/sweetalert.css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <h1>Formulario de Egresados</h1>
    <div class="contenedor">


        
        <div class="input-contenedor">
            <i class='fas fa-id-card icon'></i>
            <asp:TextBox ID="txtIdest" runat="server" placeholder="Identificación"></asp:TextBox>
        </div>
        <div class="input-contenedor">
            <i class="fas fa-user icon"></i>
            <asp:TextBox ID="txtNombre" runat="server" placeholder="Nombre"></asp:TextBox>
        </div>

        <div class="input-contenedor">
            <i class="fas fa-user icon"></i>
            <asp:TextBox ID="txtApellidos" runat="server" placeholder="Apellidos"></asp:TextBox>
        </div>

        <div class="input-contenedor">
            <i class="fas fa-phone icon"></i>
            <asp:TextBox ID="txtTelefono" runat="server" placeholder="Telefono de contacto"></asp:TextBox>
        </div>

        <div class="input-contenedor">
            <i class='fas fa-envelope icon'></i>
            <asp:TextBox ID="txtCorreo" runat="server" placeholder="Correo Electrónico"></asp:TextBox>
        </div>

        <div class="input-contenedor">
            <i class="fas fa-hammer icon"></i>
            <asp:TextBox ID="txtEstLaboral" runat="server" placeholder="Estado Laboral"></asp:TextBox>
        </div>
        <div class="input-contenedor">
            <i class="fas fa-industry icon"></i>
            <asp:TextBox ID="txtEmpresa" runat="server" placeholder="Empresa"></asp:TextBox>
            <a href="EmpresaGraduacion.aspx" class="link">Registrar nueva empresa</a>
        </div>

        <div class="input-contenedor">
            <i class="fas fa-id-card icon"></i>
            <asp:TextBox ID="txtpuesto" runat="server" placeholder="Puesto"></asp:TextBox>
        </div>
        <div class="input-contenedor">
            <i class='fas fa-university icon'></i>
            <asp:DropDownList ID="ddlUniversidad" runat="server">
                <asp:ListItem>Elige una Universidad...</asp:ListItem>
                <asp:ListItem Value="UCR">Universidad de Costa Rica</asp:ListItem>
                <asp:ListItem Value="UNA">Universidad Nacional</asp:ListItem>
                <asp:ListItem Value="TEC">Tecnológico de Costa Rica</asp:ListItem>
                <asp:ListItem Value="UL">Universidad Latina</asp:ListItem>
                <asp:ListItem Value="UCA">Universidad Florencio del Castillo</asp:ListItem>
                <asp:ListItem Value="CENFO">Universidad Cenfotec</asp:ListItem>
                <asp:ListItem Value="UAM">Universidad Americana</asp:ListItem>
                <asp:ListItem Value="ULACI">Universidad Ulacit</asp:ListItem>
                <asp:ListItem Value="UNED">Universidad a Distancia</asp:ListItem>
                <asp:ListItem Value="UACA">Universidad Autonoma de Centro América</asp:ListItem>
                <asp:ListItem Value="UFIDE">Universidad Fidélitas</asp:ListItem>
                <asp:ListItem Value="UTN">Universidad Técnica Nacional</asp:ListItem>
                <asp:ListItem Value="OTRA">Otra</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="input-contenedor">
            <i class='fas fa-list icon'></i>
            <asp:DropDownList ID="ddlCarreras" runat="server">
                <asp:ListItem>Elige una carrera...</asp:ListItem>
                <asp:ListItem Value="AE">Administración de Empresas</asp:ListItem>
                <asp:ListItem Value="DT">Mecanica Dental</asp:ListItem>
                <asp:ListItem Value="EL">Electronica</asp:ListItem>
                <asp:ListItem Value="IC">Investigación Criminal</asp:ListItem>
                <asp:ListItem Value="SE">Secretariado</asp:ListItem>
                <asp:ListItem Value="TI">Tecnologías de la Información</asp:ListItem>
                <asp:ListItem Value="TR">Turismo</asp:ListItem>
            </asp:DropDownList>
        </div>
        <asp:Button ID="btnInscripcion" runat="server" Text="Registrar Egresado" CssClass="button" OnClick="btnInscripcion_Click" />
        <br />
        <br />
        <br />
        <asp:Button ID="btnHome" runat="server" Text="Home" CssClass="button" OnClick="btnHome_Click" />
    </div>
</asp:Content>

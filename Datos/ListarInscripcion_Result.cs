//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Datos
{
    using System;
    
    public partial class ListarInscripcion_Result
    {
        public int NumIncripcion { get; set; }
        public string IdEstudiante { get; set; }
        public System.DateTime Fecha { get; set; }
        public string CodigoCarrera { get; set; }
        public string IdTutor { get; set; }
        public string OpcionGraduacion { get; set; }
        public string CodigoEmpresa { get; set; }
    }
}

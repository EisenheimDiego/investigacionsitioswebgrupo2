﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos.RegistroEgresados
{
    public class ConexionEgresados
    {
        private fundaproyectosEntities DB;
        public ConexionEgresados()
        {
            DB = new fundaproyectosEntities();
        }

        public int insertarEgresado(string Identificacion, string nombre, string apellidos, string correo, string telefono, string estadoLaboral, string nombreEmpresa, string puesto, string universidad, string idCarrera)
        {
            DB.RegistroEgresado(Identificacion,nombre,apellidos,correo,telefono,estadoLaboral,nombreEmpresa,puesto,universidad,idCarrera);
            return DB.SaveChanges();
        }
        public List<TablaEgresado> listarEgresado()
        {
            string query = "select * from TablaEgresado";
            IEnumerable<TablaEgresado> lista = DB.Database.SqlQuery<TablaEgresado>(query);
            return lista.ToList();
        }

        public List<TablaEgresado> listarUnEgresado(string id)
        {
            string query = $"select * from TablaEgresado where Identificacion = {id}";
            IEnumerable<TablaEgresado> lista = DB.Database.SqlQuery<TablaEgresado>(query);
            return lista.ToList();
        }

        public int modificarEgresado(string Identificacion, string nombre, string apellidos, string correo, string telefono, string estadoLaboral, string nombreEmpresa, string puesto, string universidad, string idCarrera)
        {
            DB.ActualizarEgresado(Identificacion, nombre, apellidos, correo, telefono, estadoLaboral, nombreEmpresa, puesto, universidad, idCarrera);
            return DB.SaveChanges();
        }

        public int insertarEmpresaEgresado(string empresa,string correo, string telefono, string paginaWeb, string sector)
        {
            DB.RegistroEmpresaEgresado(empresa,correo,telefono,paginaWeb,sector);
            return DB.SaveChanges();
        }

        public List<TablaEmpresaEgresado> listarEmpresaEgresado()
        {
            string query = "select * from TablaEmpresaEgresado";
            IEnumerable<TablaEmpresaEgresado> lista = DB.Database.SqlQuery<TablaEmpresaEgresado>(query);
            return lista.ToList();
        }

        public int modificarEmpresaEgresado(string empresa, string correo, string telefono, string paginaWeb, string sector)
        {
            DB.ActualizarEmpresaEgresado(empresa, correo, telefono, paginaWeb, sector);
            return DB.SaveChanges();
        }
        public int insertarUniversidad(string codigo, string universidad)
        {
            DB.RegistroUniversidad(codigo, universidad);
            return DB.SaveChanges();
        }

        public List<TablaUniversidad> listarUniversidad()
        {
            string query = "select Universidad from TablaUniversidad";
            IEnumerable<TablaUniversidad> lista = DB.Database.SqlQuery<TablaUniversidad>(query);
            return lista.ToList();
        }


    }
}

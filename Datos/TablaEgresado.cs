//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Datos
{
    using System;
    using System.Collections.Generic;
    
    public partial class TablaEgresado
    {
        public string Identificacion { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string EstadoLaboral { get; set; }
        public string NombreEmpresa { get; set; }
        public string Puesto { get; set; }
        public string IdUniversidad { get; set; }
        public string IdCarrera { get; set; }
    
        public virtual TablaCarrera TablaCarrera { get; set; }
        public virtual TablaUniversidad TablaUniversidad { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace Datos.RegistroEstudiantes
{
    public class ConexionEstudiantes
    {
        private fundaproyectosEntities DB;

        //Todos los metodos para la inscripcion de los estudiantes a la opcion de carrera


        //constructor
        public ConexionEstudiantes()
        {
            DB = new fundaproyectosEntities();
        }

        public int insertarEstudiante(string Identificacion, string nombre, string apellidos, string correo, string telefono)
        {
            DB.RegistroEstudiante(Identificacion, nombre, apellidos, correo, telefono);
            return DB.SaveChanges();
        }



        public List<TablaEstudiante> listarEstudiante()
        {
            string query = "select * from TablaEstudiante";
            IEnumerable<TablaEstudiante> lista = DB.Database.SqlQuery<TablaEstudiante>(query);
            return lista.ToList();
        }

        public List<TablaEstudiante> listarUnEstudiante(string id)
        {
            string query = $"select * from TablaEstudiante where Identificacion = {id}";
            IEnumerable<TablaEstudiante> lista = DB.Database.SqlQuery<TablaEstudiante>(query);
            return lista.ToList();
        }

        public int modificarEstudiante(string Identificacion, string nombre, string apellidos, string telefono, string correo)
        {
            DB.ActualizarEstudiante(Identificacion, nombre, apellidos, correo, telefono);
            return DB.SaveChanges();
        }

        public int insertarEmpresa( string nombreEmpresa, string Direccion, string telefono, string correo, string encargado)
        {
            DB.RegistroEmpresa(nombreEmpresa,Direccion,correo,telefono,encargado);
            return DB.SaveChanges();
        }

        public List<TablaEmpresa> listarEmpresa()
        {
            string query = "select * from TablaEmpresa";
            IEnumerable<TablaEmpresa> lista = DB.Database.SqlQuery<TablaEmpresa>(query);
            return lista.ToList();
        }
        public List<TablaEmpresa> listarEmpresa(string nombre)
        {
            string query = $"select * from TablaEmpresa where Empresa == {nombre}";
            IEnumerable<TablaEmpresa> lista = DB.Database.SqlQuery<TablaEmpresa>(query);
            return lista.ToList();
        }

        public int modificarEmpresa(string IdEmpresa, string nombreEmpresa, string Direccion, string telefono, string correo, string encargado, string horario)
        {
            DB.ActualizarEmpresa(IdEmpresa, nombreEmpresa, Direccion, correo, telefono, encargado, horario);
            return DB.SaveChanges();
        }

        public List<TablaOpcionGraduacion> listarOpcionGraduacion()
        {
            string query = "select OpcionGraduacion from TablaOpcionGraduacion";
            IEnumerable<TablaOpcionGraduacion> lista = DB.Database.SqlQuery<TablaOpcionGraduacion>(query);
            return lista.ToList();
        }

        public List<TablaCarrera> listarCarreras()
        {
            string query = "select Carrera from TablaCarrera";
            IEnumerable<TablaCarrera> lista = DB.Database.SqlQuery<TablaCarrera>(query);
            return lista.ToList();
        }

        public int insertarTutor(string Identificacion, string nombre, string apellidos, string carrera)
        {
            DB.RegistroTutor(Identificacion, nombre, apellidos, carrera);
            return DB.SaveChanges();
        }

        public List<TablaTutor> listarTutor()
        {
            string query = "select * from TablaTutor";
            IEnumerable<TablaTutor> lista = DB.Database.SqlQuery<TablaTutor>(query);
            return lista.ToList();
        }

        public int modificarTutor(string Identificacion, string nombre, string apellidos, string carrera)
        {
            DB.ActualizarTutor(Identificacion, nombre, apellidos, carrera);
            return DB.SaveChanges();
        }

        public int insertarInscripcion(string IdEstudiante, int ano, string cuatri ,string CodigoCarrera, string IdTutor,string OpcionGraduacion, string Empresa) //esto es para la inscripcion
        {
            DB.RegistroInscripcion(IdEstudiante, ano, cuatri, CodigoCarrera, IdTutor, OpcionGraduacion, Empresa);
            return DB.SaveChanges();
        }

        public int insertarExamen(string IdEstudiante, int ano, string cuatri, string CodigoCarrera, string IdTutor) //esto es para la inscripcion por examen
        {
            DB.RegistroExamen(IdEstudiante, ano, cuatri, CodigoCarrera, IdTutor);
            return DB.SaveChanges();
        }

        public List<TablaInscripcion> listarInscripcion()
        {
            string query = "select * from TablaInscripcion";
            IEnumerable<TablaInscripcion> lista = DB.Database.SqlQuery<TablaInscripcion>(query);
            return lista.ToList();
        }

        public int modificarInscripcion(string IdEstudiante, int ano, string cuatri, string CodigoCarrera, string IdTutor, string OpcionGraduacion, string CodigoEmpresa)
        {
            DB.ActualizarInscripcion(IdEstudiante,ano, cuatri,CodigoCarrera,IdTutor, OpcionGraduacion, CodigoEmpresa);
            return DB.SaveChanges();
        }


     
    }
}

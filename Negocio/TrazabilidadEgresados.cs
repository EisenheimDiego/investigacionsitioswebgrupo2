﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos.RegistroEgresados;
using Datos;

namespace Negocio
{
    public class TrazabilidadEgresados
    {
        ConexionEgresados conEgre = new ConexionEgresados();

        public int InsertarEgresado(string id, string nom, string ape, string corr, string tel, string estadoLaboral, string empresa, string puesto, string univ, string carrera)
        {
            int resultado = conEgre.insertarEgresado(id, nom, ape, corr, tel, estadoLaboral, empresa, puesto, univ, carrera);
            return resultado;
        }


        public int ModificarEgresado(string id, string nom, string ape, string corr, string tel, string estadoLaboral, string empresa, string puesto, string univ, string carrera)
        {
            int resultado = conEgre.modificarEgresado(id, nom, ape, corr, tel, estadoLaboral,empresa,puesto,univ,carrera);
            return resultado;
        }

        public List<TablaEgresado> ConsultarEgresado()//
        {
            return conEgre.listarEgresado();

        }

        public List<TablaEgresado> ConsultarUnEstudiante(string Id)//
        {
            return conEgre.listarUnEgresado(Id);
        }

        public List<TablaUniversidad> ConsultarUniversidad()//
        {
            return conEgre.listarUniversidad();
        }

        public int InsertarUniversidad(string codigo, string universidad)
        {
            int resultado = conEgre.insertarUniversidad(codigo,universidad);
            return resultado;
        }

        public int InsertarEmpresaEgresado(string empresa, string corr, string tel, string paginaWeb, string sector)
        {
            int resultado = conEgre.insertarEmpresaEgresado(empresa, corr, tel, paginaWeb, sector);
            return resultado;
        }


        public int ModificarEmpresaEgresado(string empresa, string corr, string tel, string paginaWeb, string sector)
        {
            int resultado = conEgre.modificarEmpresaEgresado(empresa, corr, tel, paginaWeb, sector);
            return resultado;
        }

        public List<TablaEmpresaEgresado> ConsultarEmpresaEgresado()//
        {
            return conEgre.listarEmpresaEgresado();

        }
    }
    
}

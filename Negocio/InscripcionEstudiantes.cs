﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Datos.RegistroEstudiantes;

namespace Negocio
{
    
    public class InscripcionEstudiantes
    {

        ConexionEstudiantes conEstu = new ConexionEstudiantes();

        public int InsertarEstudiante(string id, string nom, string ape, string corr, string tel)
        {
            int resultado = conEstu.insertarEstudiante(id, nom, ape, corr, tel);
            return resultado;
        }


        public int ModificarEstudiante(string id, string nom, string ape, string tel, string corr)
        {
            int resultado = conEstu.modificarEstudiante(id, nom, ape, tel, corr);
            return resultado;
        }

        public List<TablaEstudiante> ConsultarEstudiante()//
        {
            return conEstu.listarEstudiante();

        }

        public List<TablaEstudiante> ConsultarUnEstudiante(string Id)//
        {
            return conEstu.listarUnEstudiante(Id);

        }

        public int InsertarTutor(string id, string nom, string ape, string carr)
        {
            int resultado = conEstu.insertarTutor(id, nom, ape, carr);
            return resultado;
        }

        public int ModificarTutor(string id, string nom, string ape,string carr)
        {
            int resultado = conEstu.modificarTutor(id, nom, ape, carr);
            return resultado;
        }

        public List<TablaTutor> ConsultarTutor()
        {
            return conEstu.listarTutor();
        }

        public int InsertarInscripcion(string IdEstudiante, int ano, string cuatri, string CodigoCarrera, string IdTutor,string OpcionGraduacion, string CodigoEmpresa)//Esto es para la inscripcion
        {
            int resultado = conEstu.insertarInscripcion(IdEstudiante, ano, cuatri, CodigoCarrera, IdTutor, OpcionGraduacion, CodigoEmpresa);
            return resultado;
        }

        public int InsertarExamen(string IdEstudiante, int ano, string cuatri, string CodigoCarrera, string IdTutor)//Esto es para la inscripcion
        {
            int resultado = conEstu.insertarExamen(IdEstudiante, ano,cuatri, CodigoCarrera, IdTutor);
            return resultado;
        }

        public int ModificarInscripcion(string IdEstudiante, int ano, string cuatri, string CodigoCarrera, string IdTutor, string OpcionGraduacion, string CodigoEmpresa)
        {
            int resultado = conEstu.modificarInscripcion(IdEstudiante, ano,cuatri, CodigoCarrera, IdTutor, OpcionGraduacion, CodigoEmpresa);
            return resultado;
        }

        public List<TablaInscripcion> ConsultarInscripcion()
        {
            return conEstu.listarInscripcion();
        }


        public int InsertarEmpresa(string nombreEmpresa, string Direccion, string telefono, string correo, string encargado)//Esto es para la inscripcion
                                                                                                              
        {
            int resultado = conEstu.insertarEmpresa(nombreEmpresa, Direccion, correo, telefono, encargado);
            return resultado;
        }

        public int ModificarEmpresa(string IdEmpresa, string nombreEmpresa, string Direccion, string telefono, string correo, string encargado, string horario)
        {
            int resultado = conEstu.modificarEmpresa(IdEmpresa, nombreEmpresa, Direccion, correo, telefono, encargado, horario);
            return resultado;
        }

        public List<TablaEmpresa> ConsultarEmpresa()
        {
            return conEstu.listarEmpresa();
        }

        public List<TablaCarrera> ConsultarCarreras()
        {
            return conEstu.listarCarreras();
        }

        public List<TablaOpcionGraduacion> ConsultarOpcionGraduacion()
        {
            return conEstu.listarOpcionGraduacion();
        }

    }
}
